import pytest


# ----------- Flip numbers -----------
def ReverseDigits(num):
    rev_num = 0
    while (num > 0):
        rev_num = rev_num * 10 + num % 10
        num = num // 10
        print(num)
    return rev_num


# ----------- Pytest_1 -----------
def test_Reverse_Digits_1():
    assert ReverseDigits(19) == 91


def test_Reverse_Digits_2():
    assert ReverseDigits(121) == 121


def test_Reverse_Digits_3():
    assert ReverseDigits(190) == 91


def test_Reverse_Digits_4():
    assert ReverseDigits(734) == 437


# ----------- Addition of numbers -----------
def ReverseandAdd(num):
    i = 0
    while (num <= 4999999999):
        i += 1
        rev_num = ReverseDigits(num)
        num = num + rev_num
        if (ReverseDigits(num) == num):
            return num, i
        else:
            if (num > 4999999999):
                return 'Nope'


# ----------- Pytest_2 -----------
def test_Reverse_Add_1():
    assert ReverseandAdd(19) == (121, 2)


def test_Reverse_Add_2():
    assert ReverseandAdd(121) == (242, 1)


def test_Reverse_Add_3():
    assert ReverseandAdd(190) == (45254, 7)


def test_Reverse_Add_4():
    assert ReverseandAdd(196) == 'Nope'


# ----------- Input check -----------
def Prover(num):
    while True:
        try:
            num = int(num)
            return num
        except ValueError:
            raise ValueError('Вы ввели не целое число.')


# ----------- Pytest_3 -----------
def test_Prover_1():
    assert Prover(14) == 14


def test_Prover_2():
    assert Prover(436) == 436


def test_Prover_3():
    with pytest.raises(ValueError):
        Prover('gdsgdg')


def test_Prover_4():
    with pytest.raises(ValueError):
        Prover('bld')


def main():
    while True:
        try:
            num = input('Введите целое число: ')
            num = Prover(num)
            break
        except ValueError:
            print('Вы ввели не целое число.')
    num = ReverseandAdd(num)
    print(num)


if __name__ == '__main__':
    main()
